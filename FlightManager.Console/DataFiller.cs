﻿using FlightManager.Lib.Model;
using System;
using System.Collections.Generic;

namespace FlightManager.ConsoleProj
{
	internal class DataFiller
	{
		internal static List<Passenger> CreatePassengers()
		{
			return new List<Passenger>()
			{
				new Passenger
				{
					BirthDate = new DateTime(1984,12,12),
					FirstName = "Anna",
					LastName = "X",
					IssueDate = new DateTime(2018, 1,21),
					IssuePlace = "Moscow",
					PassportNumber = "ldsk232ldsfds"
				},
				new Passenger
				{
					BirthDate = new DateTime(1990,2,13),
					FirstName = "Alex",
					LastName = "N",
					IssueDate = new DateTime(2019,5,12),
					IssuePlace = "Minsk",
					PassportNumber = "PB0ksdfksd"
				},
				new Passenger
				{
					BirthDate = new DateTime(1999,7,25),
					FirstName = "Olga",
					LastName = "P",
					IssueDate = new DateTime(2021,7,25),
					IssuePlace = "Kiev",
					PassportNumber = "XXXPPPAAA"
				},
			};
		}

		internal static IEnumerable<Flight> CreateFlights()
		{
			yield return new Flight
			{
				AirLineName = "Aeroflot",
				ArrivalPort = "DAL",
				DepartureTime = new DateTime(2022, 2, 5, 3, 15, 0),
				SeatsNumber = 150,
				FreeTicketsNumber = 150
			};
			yield return new Flight
			{
				AirLineName = "Lufthansa",
				ArrivalPort = "HEL",
				DepartureTime = new DateTime(2022, 2, 15, 18, 0, 0),
				SeatsNumber = 85,
				FreeTicketsNumber = 85
			};
		}
	}
}
