﻿using System;
using System.Collections.Generic;
using FlightManager.Lib.Core;
using FlightManager.Lib.Model;
using System.Linq;

namespace FlightManager.ConsoleProj
{
	internal class Program
	{
		private static readonly TicketSaleManager TicketSaleManager = new ();
		private static readonly TicketManager TicketManager = new ();
		private static readonly PassengerManager PassengerManager = new ();
		private static readonly Lib.Core.FlightManager FlightManager = new Lib.Core.FlightManager();
		
		static void Main(string[] args)
		{
			Init();
			ShowMenu();
		}

		private static void Init()
		{
			foreach (var passenger in DataFiller.CreatePassengers())
			{
				PassengerManager.Create(passenger);
			}

			foreach (var flight in DataFiller.CreateFlights())
			{
				FlightManager.Create(flight);
				TicketManager.CreateTicketsForFlight(flight);
				TicketManager.SaveInitialTicketsToDatastore(flight);
			}
		}

		private static void ShowMenu()
		{
			Console.WriteLine();
			Console.WriteLine("1. Найти пассажира");
			Console.WriteLine("2. Найти рейс");
			Console.WriteLine("3. Продать билет");
			Console.WriteLine("4. Вернуть билет");
			Console.WriteLine("5. Удалить всех пассажиров");
			Console.WriteLine("6. Вывести все проданные билеты по рейсу");
			Console.WriteLine("0. Выход");

			HandleMenu();
		}

		private static void HandleMenu()
		{
			ConsoleKey key = Console.ReadKey().Key;
			
			switch (key)
			{
				case ConsoleKey.D1:
					{
						FindPassenger();
						break;
					}
				case ConsoleKey.D2:
					{
						FindFlight();
						break;
					}
				case ConsoleKey.D3:
					{
						SellTicket();
						break;
					}
				case ConsoleKey.D4:
					{
						RefundTicket();
						break;
					}
				case ConsoleKey.D5:
					{
						PassengerManager.DeleteAll();
						break;
					}
				case ConsoleKey.D6:
					{
						List<Ticket> tickets = GetAllReservedTickets();
						PrintReservedTickets(tickets);
						break;
					}
				case ConsoleKey.D0:
					{
						Environment.Exit(0);
						break;
					}
			}

			ShowMenu();
		}

		private static Passenger FindPassenger()
		{
			Console.WriteLine("\nВведите номер пасспорта:");

			string passNumber = Console.ReadLine();
			Passenger passenger = PassengerManager.Find(passNumber);

			if (passenger != null)
			{
				Console.WriteLine(passenger);
			}
			else
			{
				Console.WriteLine("Пассажир не найден");
			}

			return passenger;
		}

		private static Flight FindFlight()
		{
			Console.WriteLine("\nВведите порт прибытия:");

			string arrivalPort = Console.ReadLine();
			Flight flight = FlightManager.Find(arrivalPort);

			if (flight != null)
			{
				Console.WriteLine(flight);
			}
			else
			{
				Console.WriteLine("Пассажир не найден");
			}

			return flight;
		}

		private static void SellTicket()
		{
			Passenger passenger = FindPassenger();
			Flight flight = FindFlight();
			Ticket ticket = null;
			

			if (flight != null)
			{
				ticket = TicketManager.FindFirstNotReservedTicket(flight);
			}
			

			if(passenger != null && ticket != null && flight.FreeTicketsNumber > 0)
			{
				Console.WriteLine($"\nКоличество доступных билетов на рейс {flight.ArrivalPort} {flight.FreeTicketsNumber}");
				TicketSaleManager.SellTicket(flight, passenger, ticket);
				flight.FreeTicketsNumber--;

				Console.WriteLine("Билет куплен!");

				Console.WriteLine($"Количество доступных билетов на рейс {flight.ArrivalPort} {flight.FreeTicketsNumber}");
				Console.WriteLine($"Билет:\n{ticket}");
			}
		}

		private static void RefundTicket()
		{
			Passenger passenger = FindPassenger();
			Flight flight = FindFlight();
			Ticket refundTicket = null;

			if (passenger != null && flight != null)
			{
				refundTicket = TicketManager.FindReservedTicketForCanceling(flight, passenger);
			}
			
			if (refundTicket != null)
			{
				Console.WriteLine($"Количество доступных билетов на рейс {flight.ArrivalPort} {flight.FreeTicketsNumber}");
				Console.WriteLine($"Билет для возврата:\n{refundTicket}");

				TicketSaleManager.RefundTicket(refundTicket);
				flight.FreeTicketsNumber++;
				Console.WriteLine("Билет возвращен!");

				Console.WriteLine($"Количество доступных билетов на рейс {flight.ArrivalPort} {flight.FreeTicketsNumber}");
				Console.WriteLine($"Новая информация билета:\n{refundTicket}");
			}
		}

		private static List<Ticket> GetAllReservedTickets()
		{
			List<Ticket> tickets = null;
			Flight flight = FindFlight();

			if (flight != null)
			{
				tickets = TicketManager.FindAllReservedTickets(flight);
			}
			return tickets;
		}

		private static void PrintReservedTickets(List<Ticket> tickets)
		{
			if(tickets != null)
			{
				Console.WriteLine("Проданные билеты:");
				tickets.ForEach(ticket => Console.WriteLine(ticket));
			}
			else
			{
				Console.WriteLine("Пока нет зарезервированных билетов");
			}
		}
	}
}
