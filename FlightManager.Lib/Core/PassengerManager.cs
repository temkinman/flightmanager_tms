﻿using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System;
using System.Linq;

namespace FlightManager.Lib.Core
{
	public class PassengerManager : IPassengerManager
	{
		public void Create(Passenger passenger)
		{
			DataStorage.Passengers.Add(passenger);
		}

		public void Delete(Passenger passenger)
		{
			DataStorage.Passengers.Remove(passenger);
		}

		public void DeleteAll()
		{
			DataStorage.Passengers.Clear();
		}

		public Passenger Find(string passportNumber)
		{
			return DataStorage.Passengers.FirstOrDefault(x => x.PassportNumber == passportNumber);
		}

		public Passenger Find(string firstName, string lastName)
		{
			return DataStorage.Passengers
				.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
		}

		public Passenger FindById(Guid id)
		{
			return DataStorage.Passengers.FirstOrDefault(p => p.PassengerId == id);
		}
	}
}
