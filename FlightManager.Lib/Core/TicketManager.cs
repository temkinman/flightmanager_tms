﻿using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightManager.Lib.Core
{
	public class TicketManager : ITicketManager
	{
		public void CreateTicketsForFlight(Flight flight)
		{
			for (int i = 1; i <= flight.SeatsNumber; i++)
			{
				flight.Tickets.Add(new Ticket()
				{
					FlightId = flight.FlightNumber,
					NumberOfSeat = i
				});
			}
		}

		public List<Ticket> FindAllReservedTickets(Flight flight)
		{
			return DataStorage.AllTickets[flight].Where(t => t.IsSold == true).ToList();
		}

		public Ticket FindFirstNotReservedTicket(Flight flight)
		{
			return DataStorage.AllTickets[flight].FirstOrDefault(t => t.IsSold == false);
		}

		public Ticket FindReservedTicketForCanceling(Flight flight, Passenger passenger)
		{
			return DataStorage.AllTickets[flight].FirstOrDefault(t => t.FlightId == flight.FlightNumber 
																	&& t.PassengerId == passenger.PassengerId
																	&& t.IsSold == true);
		}

		public Ticket FindTicket(Flight flight, int seatNumber)
		{
			return DataStorage.AllTickets[flight].FirstOrDefault(t => t.NumberOfSeat == seatNumber);
		}

	
		public void SaveInitialTicketsToDatastore(Flight flight)
		{
			if(!DataStorage.AllTickets.ContainsKey(flight))
			{
				DataStorage.AllTickets[flight] = flight.Tickets;
			}
		}
	}
}
