﻿using FlightManager.Lib.Model;
using System.Collections.Generic;

namespace FlightManager.Lib.Core
{
	public class DataStorage
	{
		public static Dictionary<Flight, List<Ticket>> AllTickets = new();
		internal static List<Flight> Flights = new();
		internal static List<Passenger> Passengers = new();
	}
}
