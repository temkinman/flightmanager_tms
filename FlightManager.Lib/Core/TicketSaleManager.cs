﻿using FlightManager.Lib.Abstract;
using FlightManager.Lib.Model;
using System.Collections.Generic;

namespace FlightManager.Lib.Core
{
	public class TicketSaleManager : ITicketSaleManager
	{
		public void SellTicket(Flight flight, Passenger passenger, Ticket ticket)
		{
			if (DataStorage.AllTickets.ContainsKey(flight))
			{
				ticket.IsSold = true;
				ticket.PassengerId = passenger.PassengerId;
			}
		}

		public void RefundTicket(Ticket ticket)
		{
			ticket.IsSold = false;
			ticket.PassengerId = null;
		}
	}
}
