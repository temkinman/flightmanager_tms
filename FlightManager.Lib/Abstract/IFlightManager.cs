﻿using System;
using FlightManager.Lib.Model;

namespace FlightManager.Lib.Abstract
{
	internal interface IFlightManager : IDataManager<Flight>
	{
		Flight Find(string passportNumber);

		Flight Find(Guid flightNumber);
	}
}
