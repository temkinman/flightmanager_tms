﻿using FlightManager.Lib.Model;
using System;

namespace FlightManager.Lib.Abstract
{
	internal interface IPassengerManager: IDataManager<Passenger>
	{
		Passenger Find(string passportNumber);

		Passenger Find(string firstName, string lastName);
		Passenger FindById(Guid id);
	}
}
