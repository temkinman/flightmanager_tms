﻿using FlightManager.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightManager.Lib.Abstract
{
	public interface ITicketManager
	{
		public void CreateTicketsForFlight(Flight flight);
		public void SaveInitialTicketsToDatastore(Flight flight);
		public Ticket FindTicket(Flight flight, int seatNumber);
		public Ticket FindFirstNotReservedTicket(Flight flight);
		public Ticket FindReservedTicketForCanceling(Flight flight, Passenger passenger);
		public List<Ticket> FindAllReservedTickets(Flight flight);
	}
}
