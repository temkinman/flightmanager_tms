﻿using FlightManager.Lib.Model;
using System.Collections.Generic;

namespace FlightManager.Lib.Abstract
{
	public interface ITicketSaleManager
	{
		public void SellTicket(Flight flight, Passenger passenger, Ticket ticket);
		public void RefundTicket(Ticket ticket);
	}
}
