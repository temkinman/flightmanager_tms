﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightManager.Lib.Model
{
	public class Ticket
	{
		public Guid TicketId { get; set; } = Guid.NewGuid();
		public int NumberOfSeat { get; set; }
		public bool IsSold { get; set; }
		public Guid? PassengerId { get; set; }
		public Guid FlightId { get; set; }

		public override string ToString()
		{
			string status = IsSold ? "продан" : "свободный";
			return $"id: {TicketId}\nместо: {NumberOfSeat} статус: {status} passengerId: {PassengerId}";	
		}
	}
}
